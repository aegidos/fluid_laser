import javax.media.opengl.*;
import processing.opengl.*;
import jsyphon.*; // Syphon
import msafluid.*;
import java.awt.Polygon;
import processing.video.*;
import java.awt.geom.*;
import javax.vecmath.Vector2d;
import JMyron.*;
import javax.media.opengl.*;

final float FLUID_WIDTH = 120;

float invWidth, invHeight;    // inverse of screen dimensions
float aspectRatio, aspectRatio2;

MSAFluidSolver2D fluidSolver;

ParticleSystem particleSystem;

PImage imgFluid;
boolean drawFluid = true;
/*Laser_marker variables*/
int color_threshold = 200;
int brush_width = 0*(6-2);
int drip_width = brush_width/3;
int drips_probability = 15;
int zcall = -50;

JMyron jmyron;
PImage cam_image;
int cam_width = 320;
int cam_height = 240;

int[][] coordinates_projector = new int[4][2];
int[][] coordinates_cleararea = new int[4][2];

int current_color = 0;
int[] colors = {#ffffff, #ff1493, #4682b4, #9370db, #ff4500, #ffff00, #c0c0c0};

boolean pointer_on_screen;
boolean pointer_is_moving;
boolean pointer_is_visible;
int[] pointer = new int[2];
int[] pointer_old = new int[2];
int[] laser_coordinates = new int[2];
int[] pointer_camera_coordinates = new int[2];

//used for the actual drawing
ArrayList drips = new ArrayList();
int drips_position;

//helpers for calibration
int calibration_point; //used for running through the four edges during calibration
int cleararea_calibration_point; //used for running through the four edges during calibration
Point2D[] a_quad = new Point2D[4];

boolean should_draw_menu = true;
boolean should_draw_outline = false;
boolean should_draw_framerate = false;
boolean should_draw_fatmarker = true;
boolean should_draw_drips = true;
boolean should_use_mouse = false;
boolean should_draw_left = false;

//rotation
boolean wannarotate = false;
float crap = 0.0;
Point2D.Double intersection;
/*Laser_marker variables end*/

JSyphonServer mySyphon;
PGraphicsOpenGL pgl;
GL gl;
int[] texID;

void setup() {
  size(screen.width, screen.height, OPENGL);
  //  size(960, 640, OPENGL);
  hint( ENABLE_OPENGL_4X_SMOOTH );    // Turn on 4X antialiasin
  
  pgl = (PGraphicsOpenGL) g;
  gl = pgl.gl;
  initSyphon(gl,"processing");
  /*initialize jmyron Laser_marker objects menues and calibration*/
  jmyron = new JMyron();//make a new instance of the object
  jmyron.start(cam_width, cam_height);//start a capture at 320x240
  jmyron.trackColor(0,255,0,color_threshold); //R, G, B, and range of similarity

  jmyron.minDensity(15); //minimum pixels in the glob required to result in a box

  cam_image = new PImage(cam_width, cam_height);
  
  a_quad[0] = new Point2D.Float(0.0,0.0);
  a_quad[1] = new Point2D.Float(1.0,0.0);
  a_quad[2] = new Point2D.Float(1.0,1.0);
  a_quad[3] = new Point2D.Float(0.0,1.0);
    
  //top left
  coordinates_projector[0][0] = 50;
  coordinates_projector[0][1] = 10;
  //top right
  coordinates_projector[1][0] = cam_width-50;
  coordinates_projector[1][1] = 65;
  //bottom left
  coordinates_projector[2][0] = cam_width-30;
  coordinates_projector[2][1] = cam_height-40;
  //bottom right
  coordinates_projector[3][0] = 5;
  coordinates_projector[3][1] = cam_height-15;
  
  pointer_on_screen = false;
  pointer_is_moving = false;
  
  PFont f = loadFont("Univers66.vlw.gz");
  textFont(f, 16);
    
  calibration_point = 4;
  cleararea_calibration_point = 4;

  drips_position = -1;
  current_color = 0;
  change_color(colors[0]);
  /*end laser_marker*/

    invWidth = 1.0f/width;
    invHeight = 1.0f/height;
    aspectRatio = width * invHeight;
    aspectRatio2 = aspectRatio * aspectRatio;

    // create fluid and set options
    fluidSolver = new MSAFluidSolver2D((int)(FLUID_WIDTH), (int)(FLUID_WIDTH * height/width));
    fluidSolver.enableRGB(true).setFadeSpeed(0.003).setDeltaT(0.5).setVisc(0.0001);

    // create image to hold fluid picture
    imgFluid = createImage(fluidSolver.getWidth(), fluidSolver.getHeight(), RGB);

    // create particle system
    particleSystem = new ParticleSystem();

    // init TUIO
    initTUIO();

}
void change_color(int new_color) {
  drips.add(new DripsScreen(new_color));
  drips_position += 1;
}
void draw() {
  /*laser*/
    ortho(0, width, 0, height, -5000, 5000);
  translate((width/2), (-height/2));
  drip_width = brush_width/3;
    if(should_use_mouse)
    track_mouse_as_laser();
  else
    track_laser();
      update_laser_position();
  /*laser end*/
  updateTUIO();

  fluidSolver.update();

    if(drawFluid) {
        for(int i=0; i<fluidSolver.getNumCells(); i++) {
            int d = 2;
            imgFluid.pixels[i] = color(fluidSolver.r[i] * d, fluidSolver.g[i] * d, fluidSolver.b[i] * d);
        }  
        imgFluid.updatePixels();//  fastblur(imgFluid, 2);
        image(imgFluid, 0, 0, width, height);
    } 

    particleSystem.updateAndDraw();
  renderTexture(pgl.gl);
/*Laser*/
  Iterator i = drips.iterator();
    while(i.hasNext()){
      DripsScreen d = (DripsScreen)i.next();
      d.draw();
          }
        if (!should_draw_menu) {
    zcall = 5000;
  } else {
    zcall = -1;
  }

    draw_menu();
    pushMatrix();

    draw_tracking();
    popMatrix();
}

void initSyphon(GL gl, String theName) {
    if(mySyphon!=null) {
      mySyphon.stop();
    }
    mySyphon = new JSyphonServer();
    mySyphon.test();
    mySyphon.initWithName(theName);

    // copy to texture, to send to Syphon.
    texID = new int[1];
    gl.glGenTextures(1, texID, 0);
    gl.glBindTexture(gl.GL_TEXTURE_RECTANGLE_EXT, texID[0]);
    gl.glTexImage2D(gl.GL_TEXTURE_RECTANGLE_EXT, 0, gl.GL_RGBA8, width, height, 0, gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, null);
} 

void renderTexture(GL gl) {
  gl.glBindTexture(gl.GL_TEXTURE_RECTANGLE_EXT, texID[0]);
  gl.glCopyTexSubImage2D(gl.GL_TEXTURE_RECTANGLE_EXT, 0, 0, 0, 0, 0, width, height);
  mySyphon.publishFrameTexture(texID[0], gl.GL_TEXTURE_RECTANGLE_EXT, 0, 0, width, height, width, height, false);
}

public void stop() {
  dispose();
}

void dispose() {
  println("\n\nabout to stop sketch ...");
  println("deleting textures");
  gl.glDeleteTextures(1, texID, 0);
  if(mySyphon!=null) {
    println("stopping the syphon server");
    mySyphon.stop();
  }
  println("sketch stopped, done.");
}


void mouseMoved() {
    float mouseNormX = mouseX * invWidth;
    float mouseNormY = mouseY * invHeight;
    float mouseVelX = (mouseX - pmouseX) * invWidth;
    float mouseVelY = (mouseY - pmouseY) * invHeight;

//      addForce(mouseNormX, mouseNormY, mouseVelX, mouseVelY);
}

void mousePressed() {
//    drawFluid ^= true;
}

void keyPressed() {
    switch(key) {
    case 'r': 
        renderUsingVA ^= true; 
        println("renderUsingVA: " + renderUsingVA);
        break;
    case '0':
        should_use_mouse = !should_use_mouse;
        break;
    }
    println(frameRate);
}



// add force and dye to fluid, and create particles
void addForce(float x, float y, float dx, float dy) {
    float speed = dx * dx  + dy * dy * aspectRatio2;    // balance the x and y components of speed with the screen aspect ratio

    if(speed > 0) {
        if(x<0) x = 0; 
        else if(x>1) x = 1;
        if(y<0) y = 0; 
        else if(y>1) y = 1;

        float colorMult = 5;
        float velocityMult = 30.0f;

        int index = fluidSolver.getIndexForNormalizedPosition(x, y);

        color drawColor;

        colorMode(HSB, 360, 1, 1);
        float hue = ((x + y) * 180 + frameCount) % 360;
        drawColor = color(hue, 1, 1);
        colorMode(RGB, 1);  

        fluidSolver.rOld[index]  += red(drawColor) * colorMult;
        fluidSolver.gOld[index]  += green(drawColor) * colorMult;
        fluidSolver.bOld[index]  += blue(drawColor) * colorMult;

        particleSystem.addParticles(x * width, y * height, 10);
        fluidSolver.uOld[index] += dx * velocityMult;
        fluidSolver.vOld[index] += dy * velocityMult;
    }
}

void draw_tracking() {
  //draw the normal image of the camera
  int[] img = jmyron.image();
  cam_image.loadPixels();
  arraycopy(img, cam_image.pixels);
  if (should_draw_menu) {
    cam_image.updatePixels();
  };
  image(cam_image, 0, 0, 320, 240);
  
  // Draw glob Boxes
  // First box is always the red border around the video
  int[][] b = jmyron.globBoxes();
  noFill();
  stroke(255,0,0);
  for(int i=0;i<b.length;i++){
    rect(b[i][0], b[i][1], b[i][2] , b[i][3] );
  }
  
  //draw the beamer
  noFill();
  stroke(255, 255, 255);
  strokeWeight(1);
  quad(coordinates_projector[0][0], coordinates_projector[0][1],
       coordinates_projector[1][0], coordinates_projector[1][1],
       coordinates_projector[2][0], coordinates_projector[2][1],
       coordinates_projector[3][0], coordinates_projector[3][1]
       );
  
  //draw the clear area
  stroke(255,0,0, 128);
  quad(coordinates_cleararea[0][0], coordinates_cleararea[0][1],
       coordinates_cleararea[1][0], coordinates_cleararea[1][1],
       coordinates_cleararea[2][0], coordinates_cleararea[2][1],
       coordinates_cleararea[3][0], coordinates_cleararea[3][1]
       );
  
  //draw mah lazer!!!
  if(pointer_is_visible){
    int e_size = cam_width/10;
    noStroke();
    fill(255,0,0,128);
    ellipse(pointer_camera_coordinates[0], pointer_camera_coordinates[1], e_size, e_size);
  }
}
void draw_menu(){
  pushMatrix();
  fill(255,255,255);
  noStroke();
  int x = 0;
  int y = 0;
  
  textAlign(LEFT);
  
  y += 735;
  
  ArrayList lines = new ArrayList();
  lines.add("color threshold " + color_threshold + " (./, to in/decrease");
  lines.add("brush weight: " + brush_width + " (+/- to in/decrease)");
  lines.add("drips probability: " + drips_probability +" (h/g to in/decrease - lower = more likely))");
  lines.add("");
  
  Iterator i = lines.iterator();
  String s;
  while(i.hasNext()){
    s = (String)i.next();
    y += -15;
    text(s, x, y);
  }
  
  
  textAlign(RIGHT);
  x = width-20;
  y = 0;
  
  lines = new ArrayList();
  lines.add("r - Start/continue callibration");
  lines.add("x - Start/continue callibration");
  lines.add("l - Turn screen counterclockwise");
  lines.add("m - Toggle menu");
  lines.add("c - Clear screen");
  lines.add("d - Toggle drips");
  lines.add("f - Toggle framerate");
  lines.add("b - Toggle marker");
  lines.add("a - Next colour");
  lines.add("0 (nr) - use mouse mode");
  lines.add("3 - Effin' 3D effect");
    
  i = lines.iterator();
  while(i.hasNext()){
    s = (String)i.next();
    y += 15;
    text(s, x, y);
  }

  popMatrix();
}



